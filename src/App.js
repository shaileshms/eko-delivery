import React, { useState } from 'react';
import './App.css';
import {calculateDeliveryCost} from './helpers';

const defaultNodes = 'AB1, AC4, AD10, BE3, CD4, CF2, DE1, EB3, EA2, FD1';

function App() {
  const [nodes, setNodes] = useState(defaultNodes);
  const [route, setRoute] = useState('');
  const [deliveryCost, setDeliveryCost] = useState(0);
  const [error, setError] = useState('');

  const resetError = () => {
    setError('');
  };

  const onChangeNodes = (e) => {
    resetError();
    const nodes = e.target.value;
    setNodes(nodes);
    try {
      const deliveryCost = calculateDeliveryCost(nodes, route);
      setDeliveryCost(deliveryCost);
    } catch (e) {
      setDeliveryCost(e.message);
    }
  };

  const onChangeRoute = (e) => {
    resetError();
    const route = e.target.value;
    setRoute(route);
    try {
      const deliveryCost = calculateDeliveryCost(nodes, route);
      setDeliveryCost(deliveryCost);
    } catch (e) {
      setDeliveryCost(e.message);
    }
  };

  return (
    <div className="App"><div>
        <label>Nodes:</label>
        <input
          type="text"
          onChange={onChangeNodes}
          value={nodes}
        />
      </div>
      <div>
        <label>Route:</label>
        <input
          type="text"
          onChange={onChangeRoute}
          value={route}
        />
      </div>

      <div>
          Delivery Cost:
          {deliveryCost}
          {error}
      </div>
    </div>
  );
}

export default App;
