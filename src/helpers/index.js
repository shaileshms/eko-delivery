export const splitIntoArray = (str, delimiter) =>
  str.split(delimiter).map(item => item.trim());

export const createNodesObj = str => {
  const nodes = splitIntoArray(str, ',');
  const nodesObj = nodes.reduce((obj, item) => {
    const c0 = item.charAt(0);
    const c1 = item.charAt(1);
    return {
      ...obj,
      [c0]: {
        ...(obj[c0] || {}),
        [c1]: Math.abs(item.substr(2)),
      },
    };
  }, {});
  return nodesObj;
};

export const calculateDeliveryCost = (nodesStr, routeStr) => {
  const nodesObj = createNodesObj(nodesStr);
  // A-B-E
  // nodesObj['A']['B]
  const points = routeStr.split('-');
  let sum = 0;

  for (let i = 0; i < points.length - 1; i++) {
    if (typeof nodesObj[points[i]] === 'undefined') {
      throw new Error('No Such Route');
    }
    if (typeof nodesObj[points[i]][points[i+1]] === 'undefined') {
      throw new Error('No Such Route');
    }
    sum += nodesObj[points[i]][points[i+1]];
  }

  if (Number.isNaN(sum)) {
    throw new Error('No Such Route');
  }

  return sum;
};
