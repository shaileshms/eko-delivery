import {createNodesObj, calculateDeliveryCost, calculateNoOfDeliveryRoutes} from './index';

describe('createNodesObj(...)', () => {
  it('works', () => {
    expect(createNodesObj('AB1')).toEqual({ A: { B: 1 } });
    expect(createNodesObj('AB1, AC4')).toEqual({ A: { B: 1, C: 4 } });

    expect(createNodesObj('AB1, AC4, AD10, BE3, CD4, CF2, DE1, EB3, EA2, FD1')).toEqual({
      A: { B: 1, C: 4, D: 10 },
      B: { E: 3 },
      C: { D: 4, F: 2 },
      D: { E: 1 },
      E: { B: 3, A: 2 },
      F: { D: 1 },
    });
  });
});

describe('calculateDeliveryCost(...)', () => {
  it('works', () => {
    const nodesStr = 'AB1, AC4, AD10, BE3, CD4, CF2, DE1, EB3, EA2, FD1';
    expect(calculateDeliveryCost(nodesStr, 'A-B-E')).toBe(4);
    expect(calculateDeliveryCost(nodesStr, 'A-D')).toBe(10);
    expect(calculateDeliveryCost(nodesStr, 'E-A-C-F')).toBe(8);
  });

  it('handles missing routes', () => {
    const nodesStr = 'AB1, AC4, AD10, BE3, CD4, CF2, DE1, EB3, EA2, FD1';
    expect(() => calculateDeliveryCost(nodesStr, 'A-D-F')).toThrow('No Such Route');
  });

  it('handles missing key', () => {
    const nodesStr = 'AB1, AC4, AD10, BE3, CD4, CF2, DE1, EB3, EA2, FD1';
    expect(() => calculateDeliveryCost(nodesStr, 'X-Y')).toThrow('No Such Route');
  });
});
